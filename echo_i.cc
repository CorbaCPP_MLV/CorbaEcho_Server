//
// Example code for implementing IDL interfaces in file echo.idl
//

#include "echo.hh"
#include "echo_i.h"

//
// Example implementation code for IDL interface 'Echo'
//
Echo_i::Echo_i()
{
}
Echo_i::~Echo_i()
{
}

// Methods corresponding to IDL attributes and operations
char* Echo_i::echoString(const char* mesg)
{
    qDebug() << "Invoked char* Echo_i::echoString(const char* mesg))";
    return CORBA::string_dup(mesg);
}



// End of example implementation code


