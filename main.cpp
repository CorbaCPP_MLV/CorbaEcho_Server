#include <iostream>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <QByteArray>
#include "echo.hh"
#include "echo_i.h"

using namespace std;
using namespace CORBA;

int main(int argc, char** argv)
{
    try
    {
        // Initialise the ORB.
        ORB_var orb = ORB_init(argc, argv);

        // Obtain a reference to the root POA.
        Object_var obj = orb->resolve_initial_references("RootPOA");
        PortableServer::POA_var poa = PortableServer::POA::_narrow(obj);

        // We allocate the objects on the heap.  Since these are reference
        // counted objects, they will be deleted by the POA when they are no
        // longer needed.
        Echo_i* myEcho_i = new Echo_i();


        // Activate the objects.  This tells the POA that the objects are
        // ready to accept requests.
        PortableServer::ObjectId_var myEcho_iid = poa->activate_object(myEcho_i);


        // Obtain a reference to each object and output the stringified
        // IOR to stdout
        {
            // IDL interface: Echo
            Object_var ref = myEcho_i->_this();
            String_var sior(orb->object_to_string(ref));
            std::cout << "IDL object Echo IOR = '" << (char*)sior << "'" << std::endl;

            //------------------------------------------------------------------------
            QString iorPath("/tmp/ior");
            QFile caFile(iorPath);
            caFile.open(QIODevice::WriteOnly | QIODevice::Text);

            if(!caFile.isOpen())
            {
                qDebug() << "- Error, unable to open" << iorPath << "for output";
            }
            QTextStream outStream(&caFile);
            outStream << sior;
            caFile.close();


        }



        // Obtain a POAManager, and tell the POA to start accepting
        // requests on its objects.
        PortableServer::POAManager_var pman = poa->the_POAManager();
        pman->activate();

        orb->run();
        orb->destroy();
    }
    catch(TRANSIENT&)
    {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the "
                  << "server." << std::endl;
    }
    catch(SystemException& ex)
    {
        std::cerr << "Caught a " << ex._name() << std::endl;
    }
    catch(Exception& ex)
    {
        std::cerr << "Caught Exception: " << ex._name() << std::endl;
    }
    return 0;
}
