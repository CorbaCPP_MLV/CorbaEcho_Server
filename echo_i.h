#ifndef ECHO_I_H
#define ECHO_I_H

#include <iostream>
#include <QDebug>
#include "echo.hh"

using namespace std;
//
// Example class implementing IDL interface Echo
//
class Echo_i : public POA_Echo
{
private:
    // Make sure all instances are built on the heap by making the
    // destructor non-public
    //virtual ~Echo_i();

public:
    // standard constructor
    Echo_i();
    virtual ~Echo_i();

    // methods corresponding to defined IDL attributes and operations
    char* echoString(const char* mesg);
};

#endif // ECHO_I_H
