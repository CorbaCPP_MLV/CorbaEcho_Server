TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
QT += core
#CONFIG -= qt
LIBS += -lomniORB4 -lomnithread -lomniDynamic4


SOURCES += main.cpp \
    echo_i.cc \
    echoSK.cc

HEADERS += \
    echo.hh \
    echo_i.h
